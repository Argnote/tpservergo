package main
import (
 "fmt"
 "net/http"
 "time"
 "io/ioutil"
 "os"
)

func main() {
	http.HandleFunc("/", defaultHandler)
	http.HandleFunc("/add", addHandler)
	http.HandleFunc("/entries", entriesHandler)
	http.ListenAndServe(":4567", nil)
}

func defaultHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		date := time.Now()
		fmt.Fprintf(w, date.Format("15h04"))
	}
}

func addHandler(w http.ResponseWriter, req *http.Request){
	switch req.Method {
		case http.MethodPost:
			if err := req.ParseForm(); err != nil {
				fmt.Println(err)
				fmt.Fprintln(w, err)
				return
			}

			if req.PostForm["author"] != nil && req.PostForm["entry"] != nil{

				file, err := os.OpenFile("save.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0600)
				defer file.Close()
			
				if err != nil {
					panic(err)
					fmt.Fprintf(w, "Une erreur est survenue")
				}
			
				_, err = file.WriteString(req.PostForm["entry"][0]+"\n")
				if err != nil {
					panic(err)
					fmt.Fprintf(w, "Une erreur est survenue")
				}
				fmt.Fprintf(w, "%s",req.PostForm["author"][0] +": "+ req.PostForm["entry"][0])
			}else{
				fmt.Fprintf(w, "Les champs author ou entry ne sont pas renseignés")
		}
	}
}

func entriesHandler(w http.ResponseWriter, req *http.Request){
	switch req.Method {
		case http.MethodGet:
			data, err := ioutil.ReadFile("save.txt") // lire le fichier text.txt
			if err != nil {
				fmt.Println(err)
			}

			fmt.Fprintf(w, string(data))
	}
}